====== Unofficial Tildes Chats ======

Sorted roughly by the level of use they see:

  * Discord Server: https://discord.gg/jpp5TqG (very active)
  * Keybase Team (E2E encryption): https://keybase.io/team/tildeschat (pretty active)
  * IRC: https://webchat.freenode.net/?channels=%23tildes&uio=Mj10cnVlJjk9dHJ1ZSYxMT0yMzYa9 (somewhat active)
  * Telegram Group: https://t.me/joinchat/C4fK0kYDBoFpNtYPogbRoQ (dead)

**Please remember that these are not endorsed by Tildes, and no one involved with the site has any control or responsibility over what happens within. Do not bring up the unofficial chats to Deimos.**