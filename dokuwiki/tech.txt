====== ~tech ======
Technology news and discussions, intended for a general audience. Posts requiring deep technical knowledge should go in more-specific groups.

===== Notable Topics =====

  * [[tech:~FOSS Alternatives to popular services]]
  * [[tech:~Recommended Extensions from the Tildes Community]]