====== 100‐Word Writing Challenge ======

Every ​week, a prompt is posted for the other writing waves to inspire a reply of //exactly// 100 words. Not “about” or “less than”: **exactly.** These used to be called “drabbles” in the fanfic community (until the term got genericized to mean any microfiction), and they pose a fun kind of constraint whilst being just easy enough to include everyone who may wish to participate. It can be prose, poetry, anything you’d like.

The topic poster will arbitrarily pick a favorite after about five days have passed, and that person will be tasked with creating next week’s thread with their own prompt. Then they’ll pick a favorite, and it goes on‐and‐on in an endless chain. The person who made the topic doesn’t get to participate that week.