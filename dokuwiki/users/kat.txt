====== @Kat ======

A miscellaneous human being with aspirations of being a starving artist.

**Name:** Kat Suricata\\
https://KatSuricata.com/

**Tildes profile:** https://tildes.net/user/Kat\\
**Writing blog:** https://creativeartifice.com/\\
**Contact information:** [[https://katsuricata.com/contact.txt|contact.txt]]\\
**Public keys:** [[https://katsuricata.com/publickeys.txt|publickeys.txt]]