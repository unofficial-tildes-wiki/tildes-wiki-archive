====== ~comp ======
Topics focused on the more technical side of computers, things of interest to programmers, sysadmins, etc.

==== Notable Topics ====

/* Topics that have gained a lot of interest (preferably comments) in no particular order.
   You can sort by most comments to find these easily:
   https://tildes.net/~comp?order=comments*/

  * [[https://tildes.net/~comp/3t3|What operating system do you use?]]
  * [[https://tildes.net/~comp/2dh|Linux distro of choice?]]
  * [[https://tildes.net/~comp/jp|Post your setup!]]

==== Programming Challenges ====

/* If you're adding topics please list them by date posted (oldest first).
   You can sort by newest and filter by "challenge" to find out easily:
   https://tildes.net/~comp?order=new&tag=challenge */

  * [[https://tildes.net/~comp/we|Translate 24-hour time into words]]
  * [[https://tildes.net/~comp/ze|Undo this "Caesar" cipher]]
  * [[https://tildes.net/~comp/25q|Creative FizzBuzz]]
  * [[https://tildes.net/~comp/2cg|Implementing bitwise operators]]
  * [[https://tildes.net/~comp/2ob|Given a triangle of numbers, find the path from top to bottom with the largest sum]]
  * [[https://tildes.net/~comp/2yo|Anagram checking]]
  * [[https://tildes.net/~comp/2zz|Markov Chain Text Generator]]
  * [[https://tildes.net/~comp/387|Construct and traverse a binary tree]]
  * [[https://tildes.net/~comp/3ip|Freestyle textual analysis]]
  * [[https://tildes.net/~comp/3q2|Let's build some AI]]
  * [[https://tildes.net/~comp/4hw|Making our own data format]]
  * [[https://tildes.net/~comp/4ya|Two Wizards algorithm challenge]]
  * [[https://tildes.net/~comp/58b|TicTacToeBot]]
  * [[https://tildes.net/~comp/5li|Make a game in 1 hour]]
  * [[https://tildes.net/~comp/628|Merge an arbitrary number of arrays in sorted order]]
  * [[https://tildes.net/~comp/6ba|Reverse Polish Notation Calculator]]
  * [[https://tildes.net/~comp/6g2|KnightBot]]
  * [[https://tildes.net/~comp/6vt|Compute the shortest path to visit all target spots on a grid]]
  * [[https://tildes.net/~comp/77v|Counting isolated regions]]
