====== Currently Airing Anime Thread Archive ======

Welcome to the storage closet

Threads currently maintained by [[users:clerical_terrors|Clerical Terrors]]

===== 2018 =====

^ Week Number ^ Dates ^ Link ^
| 44 | 26 Oct. - 2 Nov. | [[https://tildes.net/~anime/86d/this_week_in_anime_week_44_of_2018|Link]] |
| 43 | 19 Oct. - 26 Oct. | [[https://tildes.net/~anime/7xo/this_week_in_anime_week_43_of_2018|Link]] |
| 42 | 12 Oct. - 19 Oct. | [[https://tildes.net/~anime/7o7/this_week_in_anime_week_42_of_2018|Link]] |
| 41 | 5 Oct. - 12 Oct. | [[https://tildes.net/~anime/7ex/this_week_in_anime_week_41_of_2018|Link]] |