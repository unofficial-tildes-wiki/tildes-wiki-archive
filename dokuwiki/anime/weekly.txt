====== What Have You Been Watching/Reading: Weekly Thread Archive ======

----

Here's where you find out if you're the only one who's rewatched K-ON for the sixth time in a row

Thread currently maintained by [[https://tildes.net/user/Cleb|Cleb]]

===== 2018 =====

/* the exact dates don't really matter that much, for the week I usually use https://whatweekisit.com/ because fuck putting in effort - Clerical */

^ Week ^ Dates ^ Link ^
| 43 | 25 Oct. - 30 Oct. | [[https://tildes.net/~anime/811/what_have_you_been_watching_reading_this_week_anime_manga|Link]] |
| 41 | 9 Oct. - 16 Oct. | [[https://tildes.net/~anime/7je/what_have_you_been_watching_reading_this_week_anime_manga|Link]]|
| 40 | 2 Oct. - 9 Oct. | [[https://tildes.net/~anime/79x/what_have_you_been_watching_reading_this_week_anime_manga|Link]] |
| 39 | 25 Sep. - 2 Oct. | [[https://tildes.net/~anime/711/what_have_you_been_watching_reading_this_week|Link]] |
| 38 | 17 Sep. - 25 Sep. | [[https://tildes.net/~anime/6s5/what_have_you_been_watching_reading_this_week|Link]] |
| 37 | 9 Sep. - 17 Sep. | [[https://tildes.net/~anime/6gk/what_have_you_been_watching_reading_this_week|Link]] |
| 36 | 31 Aug. - 9 Sep. | [[https://tildes.net/~anime/65j/what_have_you_been_watching_reading_this_week|Link]] |
| 35 | 22 Aug. - 31 Aug. | [[https://tildes.net/~anime/5t3/what_have_you_been_watching_reading_this_week|Link]] |
| 34 | 15 Aug. - 22 Aug. | [[https://tildes.net/~anime/5cl/what_have_you_been_watching_reading_this_week|Link]] |

