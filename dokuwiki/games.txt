====== ~Games ======
//"News and discussion about games of all types - video games, tabletop games, board games, etc."//
===== Ask Threads =====
==== Game Recommendations ====
=== "What are you playing" ===
  * [[https://tildes.net/~games/s1/what_have_yo_been_playing_recently|"What have yo been playing recently?"]] 2018-5-24
  * [[https://tildes.net/~games/1b6/what_are_you_playing|"What are you playing?"]] 2018-06-01
  * [[https://tildes.net/~games/2jc/what_have_you_been_playing_and_what_do_you_think_of_it|"What have you been playing, and what do you think of it?"]] 2018-06-20
  * [[https://tildes.net/~games/2tw/what_have_you_been_playing_and_what_do_you_think_of_it|"What have you been playing, and what do you think of it?"]] 2018-06-27
  * [[https://tildes.net/~games/3pt/what_games_have_you_guys_been_playing_lately_yall_stick_to_a_certain_genre_or_play_anything|What games have you guys been playing lately? Yall stick to a certain genre or play anything?]] 2018-07-22
  * [[https://tildes.net/~games/4gn/so_what_have_we_all_been_playing|So, what have we all been playing?]] 2018-08-06
=== Mobile Games ===
  * [[https://tildes.net/~games/29l/good_mobile_games|Good mobile games]] 2018-06-14
  * [[https://tildes.net/~games/49n/what_are_some_criminally_overlooked_mobile_games|What are some criminally overlooked mobile games?]] 2018-08-02
  * [[https://tildes.net/~games/4pi/mobile_games|Mobile Games?]] 2018-08-09
=== Single Player Games ===
  * [[https://tildes.net/~games/1p1/best_single_player_experience_offline_mmorpg_games|Best Single Player experience - Offline / MMORPG Games?]] 2018-06-05
  * [[https://tildes.net/~games/53h/any_good_text_adventures|Any good text adventures?]] 2018-08-16
  * [[https://tildes.net/~games/58s/fun_relaxing_singleplayer_games|Fun, Relaxing, Singleplayer Games]] 2018-08-19
=== Budget Games ===
  * [[https://tildes.net/~games/44f/great_and_cheap_games_on_nintendo_switch_that_are_hard_to_find|Great and cheap games on Nintendo Switch that are hard to find]] 2018-07-31
  * [[https://tildes.net/~games/4zr/what_are_the_best_free_games_for_the_pc_you_have_found_recently|What are the best free games for the PC you have found recently?]] 2018-08-15
=== Low Requirement Games ===
  * [[https://tildes.net/~games/1bv/what_are_good_optimised_games_for_a_cheap_gpu_of_2013|What are good optimised games for a cheap gpu of 2013]] 2018-06-02
  * [[https://tildes.net/~games/4qv/what_are_your_favorite_low_end_or_old_games|what are your favorite low-end or old games?]] 2018-08-10
=== Other ===
  * [[https://tildes.net/%7Egames/uq/must_have_switch_indie_titles|Must have Switch indie titles?]] 2018-05-25
  * [[https://tildes.net/~games/vq/whats_your_favorite_video_game_why|What's your favorite video game? Why?]] 2018-05-25
  * [[https://tildes.net/~games/yc/what_game_has_your_favorite_soundtrack|What game has your favorite soundtrack?]] 2018-05-27
  * [[https://tildes.net/~games/1pb/alternatives_to_older_games_that_were_amazing_to_play|Alternatives to older games that were amazing to play]] 2018-06-05
  * [[https://tildes.net/~games/33a/what_is_the_best_casual_game_console|What is the best casual game console?]] 2018-07-07
  * [[https://tildes.net/~games/35z/what_game_s_had_the_best_or_your_favourite_leveling_system|What game(s) had the best, or your favourite, leveling system?]] 2018-07-07
  * [[https://tildes.net/~games/3u7/ask_games_what_strategy_games_has_a_big_focus_on_terrain_tactics|Ask ~games: what strategy games has a big focus on terrain tactics?]] 2018-07-25
  * [[https://tildes.net/~games/7dp/whats_a_game_you_like_that_was_overlooked|What's a game you like that was overlooked?]] 2018-10-11/
==== Platform Experience ====
  * [[https://tildes.net/~games/2h9/any_linux_only_gamers_share_your_experience|Any Linux-only gamers? Share your experience!]]
==== Other ====

  * [[https://tildes.net/~games/3ey/whats_your_game_that_youd_really_like_to_see_mad|What's your game that you'd really like to see made?]]
===== Regular Discussion =====
==== Weekly Game Discussion ====
  * [[https://tildes.net/~games/7j6/weekly_game_discussion_1_call_of_duty_black_ops_4|Weekly game discussion 1: Call of Duty: Black Ops 4]] 2018-10-16